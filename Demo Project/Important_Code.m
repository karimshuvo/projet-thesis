% Import Image Type-1
input_dir = 'C:\Users\ShuvoKarim\Documents\projet-thesis\Demo Project\att_faces\s1\';
 
filenames = dir(fullfile(input_dir, '*.pgm'));
num_images = length(filenames);
images = [];

for i = 1:num_images
    filename = fullfile(input_dir, filenames(i).name);
    img = imread(filename);
    figure
    imshow(img);
    images(:,i) = img(:);
end
image_dims =[112, 92];

% display the eigenvectors
figure;
for n = 1:limit
subplot(2, ceil(limit/2), n);
eig_vect = reshape(leigFace(:,n), [112,92]);
imagesc(eig_vect);
colormap(gray); 
end


% Display Mean Face
B = reshape(mean_face,[112,92]);
bm=uint8(B);
imwrite(bm,'mean.pgm');
figure
imshow('mean.pgm');


% input image for test
[file, path]= uigetfile({'*.png'},'input image');
fullpath=strcat(path, file);
input_img= imread(fullpath);
% input_img = rgb2gray(input_img); 
input_img = imresize(input_img,image_dims);

clear all; close all; clc;

% input_dir = 'C:\Users\ShuvoKarim\Documents\projet-thesis\Demo Project\att_faces\s1\';
%  
% filenames = dir(fullfile(input_dir, '*.pgm'));
% num_images = length(filenames);
% images = [];
% 
% for i = 1:num_images
%     filename = fullfile(input_dir, filenames(i).name);
%     img = imread(filename);
% %     figure
% %     imshow(img);
%     images(:,i) = img(:);
% end
% image_dims =[112, 92];
input_dir = 'C:\Users\ShuvoKarim\Documents\projet-thesis\Simulation_one\trainset\';
image_dims =[120, 104];

filenames = dir(fullfile(input_dir, '*.pgm'));
num_images = length(filenames);
images = [];

% Images converted into a column vector
for n = 1:num_images
    filename = fullfile(input_dir, filenames(n).name);
    img = imread(filename);
    img = im2double(img);
    img = imresize(img,image_dims);
    images(:,n) = img(:);
end


mean_face = mean(images, 2);

imgDiff =[];

for i=1:num_images
    imgDiff(:,i)= images(:,i)-mean_face;

end

imgDiffTrans= imgDiff';

% B = reshape(mean_face,[112,92]);
% bm=uint8(B);
% imwrite(bm,'mean.pgm');
% figure
% imshow('mean.pgm');

cov_mat= imgDiffTrans*imgDiff;

[eigVec, eigVal] = eigs(cov_mat);
limit=length(eigVal);

for i=1:limit
   eig_ui(:,i)=imgDiff*eigVec(:,i); 
end
 %display the eigenvectors
% figure;
% for n = 1:limit
% subplot(2, ceil(limit/2), n);
% eig_vect = reshape(leigFace(:,n), [112,92]);
% imagesc(eig_vect);
% colormap(gray); 
% end

% Weight calculation
for i=1:num_images
    for j=1:limit
        eig_ui_t=eig_ui(:,j)';
        weight(j,i)=eig_ui_t*imgDiff(:,i);
    end
end

% input image for test
input_img= imread('C:\Users\ShuvoKarim\Documents\projet-thesis\Simulation_one\testset\7_7.png');
input_img = imresize(input_img,image_dims);

% input image difference and input image weight
input_img = im2double(input_img);
input_img_diff= input_img(:)- mean_face;

input_weight= eig_ui' * input_img_diff;

% Euclidian distance between the input image and train images
for i=1:num_images
    distance(:,i)= norm(input_weight - weight(:,i));
end



% match image score and it's index
[match_score, match_index] = min(distance);

% display the result
figure();
imshow([input_img ,reshape(images(:,match_index), image_dims)]);
colormap gray
title(sprintf('matches %s, score %f', filenames(match_index).name, match_score));
